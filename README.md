# ShockEQ
This program was developed during my Master's work, with my advisor Dr. Maurício Rosa, for the calculation of both normal and oblique shock waves using air as perfect gas and/or chemical  equilibrium using the Srinavasan and Tanehill correlations high temperature air. 

It was created in the context of hypersonics and scramjet research, so it contains the option to calculate a full scramjet intake as a sequence of ramps. Version 1.0 is fully validated for the conditions of scramjet flight, more information in this publication: https://www.researchgate.net/publication/264686940_A_computer_program_for_calculating_normal_and_oblique_shock_waves_for_airflows_in_chemical_and_thermodynamic_equilibrium.

The program is being worked on and I intend to add more features up to a full scramjet calculation. Please note this is not CFD, it merely calculates conditions accross the shock waves.

If there are any comments, suggestions or inquiries, feel free to contact me.
