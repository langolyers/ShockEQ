!    *******************************************************************
!    * This program is used to calculate results for both normal and   *
!    * oblique shock waves using air as perfect gas and/or chemical    *
!    * equilibrium. it uses the Srinivasan and Tanehill correlations   *
!    * for high temperature air.                                       *
!    *                                                                 *
!    * authors: Augusto Fontan Moura            (PhD Student - UQ)     *
!    *          Dr. Mauricio Antoniazzi Pinheiro Rosa (eah-c/ieav)     *
!    *                                                                 *
!    * Created 14-06-2012                                              *
!    * Latest version available on github:                             *
!    * https://github.com/Langolyers/ShockEQ                           *
!    * v1.0                                                            *
!    *******************************************************************
!	  
      program shockeq
!     
      use srinivasan
      use normal_shock	
      use oblique_shock	  
      
	  implicit real*8(a-h,m-z)
      double precision::p1,t1,p01,rho1,a1,m1,v1,h1,beta,thetar,thetamax    !flow properties before the shock
      double precision::gama,r,cp,cv,e1                                   !properties of air before shock
      real, dimension(:), allocatable::theta                              !array to assume values of angles
      logical::realgas,perfgas                                     !conditions for calculating equilibrium or perfect gas after the shock
      logical::wave						   !condition to define type of shock, true equals normal shock
      integer::j  
      double precision::pi
      common/aaa/p2,t2,rho2,m2,v2,h2,e2,a2,p02,pr,tr,mr,p0r,dsr
!
      pi=4.d0*datan(1.d0)
!         
!     opens the files used by the program	        
      open(1,file='input.dat',status='old',access= 'sequential',action='read')
      open(2,file='perf_gas_results.dat',status='replace',action='write')
      open(3,file='real_gas_results.dat',status='replace',action='write')
      open(4,file='error_log.dat',status='replace',action='write')
!
      pr = 1.                      !ratios used for calculation of total properties ratios across all ramps
      tr = 1.
      mr = 1.
      p0r = 1.
      dsr = 0.
!      
      read(1,"(7/,7x,f10.5,/,7x,f10.3,/,7x,f8.6)")p1,t1,m1
!
!     calculation of properties before shock wave
      gama=1.4
      r=287
      cv=r/(gama-1.d0)
      cp=cv*gama
      rho1=p1/(r*t1)
      a1=sqrt(gama*r*t1)
      v1=m1*a1
      p01=p1*(1.+((gama-1.)/2.)*m1*m1)**(gama/(gama-1.))
      e1=cv*t1
      h1=cp*t1
!
      write(2,*)'        free-stream properties (before shock)'
      write(2,*)''
      write(2,*)'            pressure [pa]               temperature [k]              density [kg/m3]'
      write(2,"(12x,f13.0,15x,f15.1,14x,f15.5)")p1,t1,rho1
      write(2,*)''
      write(2,*)'           velocity [m/s]                   mach number         speed of sound [m/s]'
      write(2,"(11x,f14.2,19x,f11.2,10x,f20.2)")v1,m1,a1
      write(2,*)''
      write(2,*)'          enthalpy [j/kg]        internal energy [j/kg]          total pressure [pa]'  
      write(2,"(10x,es15.4,8x,es22.4,10x,es19.4)")h1,e1,p01
      write(2,*)''
      write(2,*)'               cp [j/kgk]                    cv [j/kgk]                    gamma'
      write(2,"(15x,f10.0,20x,f10.0,20x,f10.5)")cp,cv,gama
      write(2,*)'______________________________________________________________________________________________________'
      write(2,*)'______________________________________________________________________________________________________'
!
      write(3,*)'        free-stream properties (before shock)'
      write(3,*)''
      write(3,*)'            pressure [pa]               temperature [k]              density [kg/m3]'
      write(3,"(12x,f13.0,15x,f15.1,14x,f15.5)")p1,t1,rho1
      write(3,*)''
      write(3,*)'           velocity [m/s]                   mach number         speed of sound [m/s]'
      write(3,"(11x,f14.2,19x,f11.2,10x,f20.2)")v1,m1,a1
      write(3,*)''
      write(3,*)'          enthalpy [j/kg]        internal energy [j/kg]          total pressure [pa]'  
      write(3,"(10x,es15.4,8x,es22.4,10x,es19.4)")h1,e1,p01
      write(3,*)''
      write(3,*)'               cp [j/kgk]                    cv [j/kgk]                    gamma'
      write(3,"(15x,f10.0,20x,f10.0,20x,f10.5)")cp,cv,gama
      write(3,*)'______________________________________________________________________________________________________'
      write(3,*)'______________________________________________________________________________________________________'
!        
      read(1,"(7x,l1,/,7x,l1,/,7x,l1)")wave,perfgas,realgas
!
!     check type of shock
      if (wave) then     !normal shock wave
                if (perfgas) call norm1 (p1,t1,p01,m1,rho1,v1,gama,r,cp)   ! perfect gas
                if (realgas) call norm2 (p1,t1,rho1,v1,e1,h1)     ! equilibrium air
!
        else    !oblique shock1
                   read(1,"(7x,i2)")iramp      !reads the number of ramps
	           allocate ( theta(iramp) )   !defines the size of the theta array as the number of ramps
                   do 300 j=1,iramp
                   read(1,"(7x,f6.3)")theta(j)
 300               continue
!     the following variables store the values of the flow properties before the shock to be used in the second loop
		   p1temp=p1
		   m1temp=m1
		   t1temp=t1
		   rho1temp=rho1
		   p01temp=p01
		   h1temp=h1
		   e1temp=e1
		   a1temp=a1
		   v1temp=v1
!
		   if (perfgas) then    !check for perfect gas calculation
   		   write(2,*)''
                   write(2,*)'         oblique shock with calorically perfect gas'
                   do 100 j=1,iramp                               !first loop, to calculate 
 !                     write(4,*)'(obli1)theta(j) = ',theta(j)
                      if(j.gt.1) then
                           m1 = m2
                           p1 = p2
                           t1 = t2
                           rho1 = rho2
                           p01 = p02
                           h1 = h2
                           e1 = e2
                           a1 = a2
                           v1 = v2
                      endif
		   thetar=theta(j)*pi/180.
!	           write(4,*)'(obli1)thetar = ',thetar
		   call obli1 (m1,beta,gama,thetar,p1,rho1,t1,p01,r,cp,j)
 100               continue
		   endif
		   if (realgas) then !checks if high temperature gas calculations should be made
!     the following restores the read variables for the high temperature calculations
   		   write(3,*)''
                   write(3,*)'         oblique shock with air in chemical equilibrium'
                   p1=p1temp
		   m1=m1temp
		   t1=t1temp
		   rho1=rho1temp
		   p01=p01temp
		   h1=h1temp
		   e1=e1temp
		   a1=a1temp
		   v1=v1temp
                   do 200 j=1,iramp
!                      write(4,*)'(obli2)theta(j) = ',theta(j)
                      if(j.gt.1) then
                           m1 = m2
                           p1 = p2
                           t1 = t2
                           rho1 = rho2
                           p01 = p02
                           h1 = h2
                           e1 = e2
                           a1 = a2
                           v1 = v2
                           endif
		   thetar=theta(j)*pi/180.
!		   write(4,*)'(obli2)thetar = ',thetar
		   call obli2 (beta,thetar,p1,rho1,v1,t1,h1,e1,m1,j)
 200               continue
		   endif
      write(2,*)'______________________________________________________________________________________________________'
      write(2,*)''
      write(2,*)''
      write(2,*)'        properties across all ramps '
      write(2,"(10x,'entropy increase= ',f8.0,/)")dsr
      write(2,*)'        properties ratios'
      write(2,"(//,10x,'pressure = ',16x,f8.3,/,10x,'temperature = ',13x,f8.3,/,10x,'mach = ',20x,f8.3)")pr,tr,mr
      write(2,"(10x,'total pressure = ',10x,f8.3,//)")p0r
	              endif
      write(4,*)'      succesful run, no known errors to report.'
      end program shockeq 