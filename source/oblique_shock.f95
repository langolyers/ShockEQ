     module oblique_shock
	 
	       
	  use srinivasan
      use obliter	  
 	 
	 contains
	 
	 !_______________________________________________________________________________________________
!calculations for properties after oblique shock wave for perfect gas
      subroutine obli1 (m1,beta,gama,thetar,p1,rho1,t1,p01,r,cp,j)
	  implicit none
      double precision::p1,t1,rho1,m1,mn1,p01
      double precision::p2,p02,t2,rho2,h2,e2,a2,m2,v2,mn2
      double precision::beta,betap,thetar,thetamax
      double precision::p2p1,t2t1,rho21,ds,pr,tr,mr,p0r,dsr
      double precision::dl,dx,x0
      double precision::gama,r,cp
      double precision::pi
      integer::j
      common/aaa/p2,t2,rho2,m2,v2,h2,e2,a2,p02,pr,tr,mr,p0r,dsr
!
      pi=4.d0*datan(1.d0)
!
      thetamax=(0.0049*m1**5-0.1749*m1**4+2.4718*m1**3-17.344*m1*m1+61.678*m1-48.556)*pi/180 !melhorar cálculo de thetamax e verificar
!
         dl = 1.0e-09
         dx = pi/60.
         x0 = thetar
         call secant (dl,x0,dx,gama,m1,thetar,thetamax)
         beta = x0
!
         mn1=m1*sin(beta)
         p2p1=1.+(2.*gama/(gama+1.))*(mn1*mn1-1.)
         rho21=mn1**2.*(gama+1.)/(2.+(gama-1.)*mn1**2.)
         t2t1=p2p1/rho21
         mn2=sqrt((mn1**2.+(2./(gama-1.)))/((2.*gama/(gama-1.))*mn1**2.-1.))
!
         ds=cp*dlog(t2t1)-r*dlog(p2p1)
         p02=p01*dexp(-ds/r)
!
         m2=mn2/sin(beta-thetar)
         p2=p1*p2p1
         rho2=rho1*rho21
         t2=t1*t2t1
         h2=cp*t2
         a2=sqrt(gama*r*t2)
         v2 = m2*a2
         betap=beta*180./pi
!
         pr = pr*p2p1
         tr = tr*t2t1
         mr = mr*m2/m1
         p0r = p0r*p02/p01
         dsr = dsr+ds
!
      write(2,*)''
      write(2,*)'                      ramp #                theta                  beta                    gamma'
      write(2,"(23x,i6,16x,f5.2,17x,f5.2,20x,f10.5)")j,thetar*180/pi,betap,gama
      write(2,*)''
      write(2,*)'                       p2/p1                t2/t1             rho2/rho1'
      write(2,"('ratios',17x,f6.3,15x,f6.3,13x,f9.3,11x,f6.3,11x,f6.3)")p2p1,t2t1,rho21
      write(2,*)'-------------------------------------------------------------------------------------------------------------'
      write(2,*)''
      write(2,*)'                     p2 [pa]               t2 [k]          rho2 [kg/m3]               h2'
      write(2,"(19x,f10.0,15x,f6.1,10x,f12.5,7x,f10.0)")p2,t2,rho2,h2
      write(2,*)''
      write(2,*)'                    v2 [m/s]                   m2              a2 [m/s]              mn1              mn2' 
      write(2,"(21x,f8.2,17x,f4.2,14x,f8.2,13x,f4.2,13x,f4.2)")v2,m2,a2,mn1,mn2
      write(2,*)''
      write(2,*)'          total pressure [pa]                  entropy increase [j/kgk]' 
      write(2,"(10x,f19.0,36x,f6.0)")p02,ds
      write(2,*)''
      write(2,*)'_____________________________________________________________________________________________________________'
      !
      return
      end
      !__________________________________________________________________________________________________
      !
      !oblique shock with equilibrium air using srinivasan correlations
      subroutine obli2 (beta,thetar,p1,rho1,v1,t1,h1,e1,m1,j)
      implicit none
      double precision::p1,t1,rho1,v1,e1,h1,vn1,beta,betar,thetar,m1,a,thetamax
      double precision::p2,h2,t2,rho2,v2,e2,vn2,m2,a2,p02,p2t
      double precision::p2p1,t2t1,rho21,ro1ro2,e2e1,h2h1,pr,tr,mr,p0r,dsr
      double precision::y0
      double precision::pi,gamm
      integer::mflag,j
      common/aaa/p2,t2,rho2,m2,v2,h2,e2,a2,p02,pr,tr,mr,p0r,dsr
!

	  pi=4.d0*datan(1.d0)
!
      y0 = thetar+pi/60
!
      call secant2 (vn1,vn2,v1,p1,h1,rho1,thetar,beta,y0,p2,ro1ro2,h2,rho2,e2)
!
      beta = y0
      mflag=3
      call tgas1 (e2,rho2,p2t,a2,t2,mflag)
!
!      write(3,*)'gamma = ',gamm
      v1=vn1/sin(beta)
      p2p1=p2/p1
      t2t1=t2/t1
      e2e1=e2/e1
      h2h1=h2/h1
      rho21=1./ro1ro2
      v2 = vn2/sin(beta-thetar)
      m2=v2/a2
!
      betar=beta*180/pi
!      
      pr = pr*p2p1
      tr = tr*t2t1
      mr = mr*m2/m1
!
      write(3,*)''
      write(3,*)'                      ramp #                theta                  beta'
      write(3,"(23x,i6,16x,f5.2,17x,f5.2,20x,f10.5)")j,thetar*180/pi,betar
      write(3,*)''
      write(3,*)'                       p2/p1                t2/t1             rho2/rho1            h2/h1            e2/e1'
      write(3,"('ratios',17x,f6.3,15x,f6.3,13x,f9.3,11x,f6.3,11x,f6.3)")p2p1,t2t1,rho21,h2h1,e2e1
      write(3,*)'-------------------------------------------------------------------------------------------------------------'
      write(3,*)''
      write(3,*)'                     p2 [pa]               t2 [k]          rho2 [kg/m3]               h2               e2'
      write(3,"(19x,f10.0,15x,f6.1,10x,f12.5,7x,f10.0,7x,f10.0)")p2,t2,rho2,h2,e2
      write(3,*)''
      write(3,*)'                    v2 [m/s]                   m2              a2 [m/s]        vn1 [m/s]        vn2 [m/s]' 
      write(3,"(21x,f8.2,17x,f4.2,14x,f8.2,8x,f9.2,8x,f9.2)")v2,m2,a2,vn1,vn2
      write(3,*)''
      write(3,*)'_____________________________________________________________________________________________________________'
!
!      write(3,*)'gamma = ',gamm
      return
      end subroutine obli2
	  
	  end module oblique_shock