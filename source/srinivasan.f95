     module srinivasan
	 
	 contains
	 
	 !     subroutine for calculating equilibrium air properties using Srinivasan correlations
!     inputs for subroutine:
!
!     e2= internal energy, in (m/sec)**2 ;
!     rho2= density, in kg/m**3 .
!
!     outputs:
!
!     p2t= pressure, in newtons/m**2 ;
!     a2= speed of sound, in m/sec ;
!     t2= temperature, in kelvin .
!
!     if mflag=0, return p ;
!     if mflag=1, return p and a ;
!     if mflag=2, return p and t ;
!     if mflag=3, return p, a and t .
!
      subroutine tgas1 (e2,rho2,p2t,a2,t2,mflag)
      implicit real*8 (a-h,o-z)
      data e0,r0,p0,t0,gascon/78408.4e00,1.292e00,1.0133e05,273.15e00,287.06e00/
      rratio=rho2/r0
      eratio=e2/e0
      y=dlog10(rratio)
      z=dlog10(eratio)
      lflag=0
      kflag=0
      if (mflag.gt.1) lflag=1
      if ((mflag.eq.1).or.(mflag.eq.3)) kflag=1
      if(abs(y+4.5e00).lt.2.5e-02) go to 20
      if(abs(y+0.5e00).lt.5.0e-03) go to 50
      iflag=-1
      go to 90
10    if(lflag.eq.1) go to 300
      return
20    iflag=0
      rsave=rho2
      ym=y
      y=-4.5e00+2.5e-02
      yhigh=y
      rho2=(10.**y)*r0
      jflag=-1
      go to 90
30    phigh=p2t
      ahigh=a2
      y=-4.5e00-2.5e-02
      ylow=y
      rho2=(10.**y)*r0
      jflag=0
      go to 90
40    plow=p2t
      alow=a2
      go to 80
50    iflag=1
      rsave=rho2
      ym=y
      y=-0.5e00+0.5e-02
      yhigh=y
      rho2=(10.**y)*r0
      jflag=-1
      go to 90
60    phigh=p2t
      ahigh=a2
      y=-0.5e00-0.5e-02
      ylow=y
      rho2=(10.**y)*r0
      jflag=0
      go to 90
70    plow=p2t
      alow=a2
80    p2t=plow+(phigh-plow)/(yhigh-ylow)*(ym-ylow)
      a2=alow+(ahigh-alow)/(yhigh-ylow)*(ym-ylow)
      rho2=rsave
      if(lflag.eq.1) go to 300
      return
90    continue
      if(y.gt.-0.5e00) go to 200
      if(y.gt.-4.5e00) go to 150
      if(z.gt.0.65e00) go to 100
      gamm=1.3965e00
      go to 250
100   if(z.gt.1.5e00) go to 110
      gas1=1.52792e00-1.26953e-02*y
      gas2=(-6.13514e-01-5.08262e-02*y)*z
      gas3=(-5.49384e-03+4.75120e-05*z-3.18468e-04*y)*y*y
      gas4=(6.31835e-01+3.34012e-02*y-2.19921e-01*z)*z*z
      gas5=-4.96286e01-1.17932e+01*y
      gas6=(6.91028e01+4.40405e+01*y)*z
      gas7=(5.09249e00-1.40326e00*z+2.08988e-01*y)*y*y
      gas8=(1.37308e01-1.78726e01*y-1.86943e01*z)*z*z
      gas9=exp(24.60452e00-2.e00*y-2.093022e01*z)
      deno=1.-gas9
      gamm=gas1+gas2+gas3+gas4+(gas5+gas6+gas7+gas8)/deno
      if(kflag.eq.0) go to 260
      gas1r=-1.26953e-02
      gas2r=-5.08262e-02*z
      gas3r=(-1.098768e-02-9.50240e-05*z-9.554040e-04*y)*y
      gas4r=3.34012e-02*z*z
      gas5r=-1.17932e01
      gas6r=4.40405e01*z
      gas7r=(1.018498e01-2.80652e00*z+6.269641e-01*y)*y
      gas8r=-1.78726e01*z*z
      gas9r=-2.0e00
      gas2e=gas2/z
      gas3e=4.75120e-05*y*y
      gas4e=(1.26367e00+6.68024e-02*y-6.59763e-01*z)*z
      gas6e=gas6/z
      gas7e=-1.40326e00*y*y
      gas8e=(2.74616e01-3.57452e01*y-5.60829e01*z)*z
      gas9e=-2.093022e01
      gammr=gas1r+gas2r+gas3r+gas4r+(gas5r+gas6r+gas7r+gas8r)/deno+(gas5+gas6+gas7+gas8)*gas9r*gas9/(deno**2)
      gamme=gas2e+gas3e+gas4e+(gas6e+gas7e+gas8e)/deno+(gas5+gas6+gas7+gas8)*gas9e*gas9/(deno**2)
      go to 260
110   if(z.gt.2.2e00) go to 120
      gas1=-1.70333e01-5.08545e-01*y
      gas2=(2.46299e01+4.45617e-01*y)*z
      gas3=(-8.95298e-03+2.29618e-03*z-2.89186e-04*y)*y*y
      gas4=(-1.10204e01-9.89727e-02*y+1.62903e00*z)*z*z
      gas5=1.86797e01+5.19662e-01*y
      gas6=(-2.41338e01-4.34837e-01*y)*z
      gas7=(9.16089e-03-1.52082e-03*z+3.46482e-04*y)*y*y
      gas8=(1.02035e01+9.70762e-02*y-1.39460e00*z)*z*z
      gas9=(-1.42762e02-1.647088e00*y+7.660312e01*z+8.259346e-01*y*z)
      if(kflag.eq.0) go to 240
      gas1r=-5.08545e-01
      gas2r=4.45617e-01*z
      gas3r=(-1.790596e-02+4.59236e-03*z-8.67558e-04*y)*y
      gas4r=-9.89727e-02*z*z
      gas5r=5.19662e-01
      gas6r=-4.34837e-01*z
      gas7r=(1.832178e-02-3.04164e-03*z+1.039446e-03*y)*y
      gas8r=9.70762e-02*z*z
      gas9r=-1.647088e00+8.259346e-01*z
      gas2e=gas2/z
      gas3e=2.29618e-03*y*y
      gas4e=(-2.20408e01-1.979454e-01*y+4.88709e00*z)*z
      gas6e=gas6/z
      gas7e=-1.52082e-03*y*y
      gas8e=(2.0407e01+1.941524e-01*y-4.1838e00*z)*z
      gas9e=7.660312e01+8.259346e-01*y
      go to 240
120   if (z.gt.3.05e00) go to 130
      gas1=2.24374e00+1.03073e-01*y
      gas2=(-5.32238e-01-5.59852e-02*y)*z
      gas3=(3.56484e-03-1.01359e-04*z+1.59127e-04*y)*y*y
      gas4=(-4.80156e-02+1.06794e-02*y+3.66035e-02*z)*z*z
      gas5=-5.70378e00-3.10056e-01*y
      gas6=(5.01094e00+1.80411e-01*y)*z
      gas7=(-9.49361e-03+1.94839e-03*z-2.24908e-04*y)*y*y
      gas8=(-1.40331e00-2.79718e-02*y+1.20278e-01*z)*z*z
      gas9=(1.139755e02-4.985467e00*y-4.223833e01*z+2.009706e00*y*z)
      if(kflag.eq.0) go to 240
      gas1r=1.03073e-01
      gas2r=-5.59852e-02*z
      gas3r=(7.12968e-03-2.0218e-04*z+4.77381e-04*y)*y
      gas4r=1.06794e-02*z*z
      gas5r=-3.10056e-01
      gas6r=1.80411e-01*z
      gas7r=(-1.898722e-02+3.89678e-03*z-6.74724e-04*y)*y
      gas8r=-2.79718e-02*z*z
      gas9r=-4.985467e00+2.009706e00*z
      gas2e=gas2/z
      gas3e=-1.01359e-04*y*y
      gas4e=(-9.60312e-02+2.13588e-02*y+1.098105e-01*z)*z
      gas6e=gas6/z
      gas7e=1.94839e-03*y*y
      gas8e=(-2.80662e00-5.59436e-02*y+3.60834e-01*z)*z
      gas9e=-4.223833e01+2.009706e00*y
      go to 240
130   if(z.gt.3.4e00) go to 140
      gas1=-0.20807e02+0.40197e00*y
      gas2=(0.22591e02-0.25660e00*y)*z
      gas3=(-0.95833e-03+0.23966e-02*z+0.33671e-03*y)*y*y
      gas4=(-0.77174e01+0.4606e-01*y+0.878e00*z)*z*z
      gas5=-0.21737e03-0.46927e01*y
      gas6=(0.18101e03+0.26621e01*y)*z
      gas7=(-0.34759e-01+0.64681e-02*z-0.70391e-03*y)*y*y
      gas8=(-0.50019e02-0.38381e00*y+0.45795e01*z)*z*z
      gas9=(0.4544373e03+0.1250133e02*y-0.1376001e03*z-0.3641774e01*y*z)
      if(kflag.eq.0) go to 240
      gas1r=0.40197e00
      gas2r=-0.25660e00*z
      gas3r=(-1.91666e-03+4.7932e-03*z+1.01013e-03*y)*y
      gas4r=0.4606e-01*z*z
      gas5r=-0.46927e01
      gas6r=0.26621e01*z
      gas7r=(-6.9518e-02+1.29362e-02*z-2.11173e-03*y)*y
      gas8r=-0.38381e00*z*z
      gas9r=0.1250133e02-0.3641774e01*z
      gas2e=gas2/z
      gas3e=0.23966e-02*y*y
      gas4e=(-1.54348e01+9.212e-02*y+2.634e00*z)*z
      gas6e=gas6/z
      gas7e=0.64681e-02*y*y
      gas8e=(-1.00038e02-7.6762e-01*y+1.37385e01*z)*z
      gas9e=-0.1376001e03-0.3641774e01*y
      go to 240
140   if(z.gt.3.69e00) write(6,1000) rho2,e2
      gas1=-5.22951e01-4.00011e-01*y
      gas2=(4.56439e01+2.24484e-01*y)*z
      gas3=(-3.73775e-03+2.43161e-03*z+2.24755e-04*y)*y*y
      gas4=(-1.29756e01-2.79517e-02*y+1.22998e00*z)*z*z
      gamm=gas1+gas2+gas3+gas4
      if(kflag.eq.0) go to 260
      gas1r=-4.00011e-01
      gas2r=2.24484e-01*z
      gas3r=(-7.4755e-03+4.86322e-03*z+6.74265e-04*y)*y
      gas4r=-2.79517e-02*z*z
      gas2e=gas2/z
      gas3e=2.43161e-03*y*y
      gas4e=(-2.59512e01-5.59034e-02*y+3.68994e00*z)*z
      gammr=gas1r+gas2r+gas3r+gas4r
      gamme=gas2e+gas3e+gas4e
      go to 260
150   if(z.gt.0.65e00) go to 160
      gamm=1.398e00
      go to 250
160   if (z.gt.1.5e00) go to 170
      gas1=1.39123e00-4.08321e-03*y
      gas2=(1.42545e-02+1.41769e-02*y)*z
      gas3=(2.57225e-04+6.52912e-04*z+8.46912e-05*y)*y*y
      gas4=(6.2555e-02-7.83637e-03*y-9.78720e-02*z)*z*z
      gas5=5.80955-1.82302e-01*y
      gas6=(-9.62396e00+1.79619e-01*y)*z
      gas7=(-2.30518e-02+1.18720e-02*z-3.35499e-04*y)*y*y
      gas8=(5.27047e00-3.65507e-02*y-9.19897e-01*z)*z*z
      gas9=(-10.0e00*z+14.2e00)
      if(kflag.eq.0) go to 240
      gas1r=-4.08321e-03
      gas2r=1.41769e-02*z
      gas3r=(5.1445e-04+1.305824e-03*z+2.540736e-04*y)*y
      gas4r=-7.83637e-03*z*z
      gas5r=-1.82302e-01
      gas6r=1.79619e-01*z
      gas7r=(-4.61036e-02+2.3744e-02*z-1.006497e-03*y)*y
      gas8r=-3.65507e-02*z*z
      gas9r=0.0e00
      gas2e=gas2/z
      gas3e=6.52912e-04*y*y
      gas4e=(1.2511e-01-1.567274e-02*y-2.93616e-01*z)*z
      gas6e=gas6/z
      gas7e=1.1872e-02*y*y
      gas8e=(1.054094e01-7.31014e-02*y-2.759691e00*z)*z
      gas9e=-10.0e00
      go to 240
170   if (z.gt.2.22e00) go to 180
      gas1=-1.20784e00-2.57909e-01*y
      gas2=(5.02307e00+2.87201e-01*y)*z
      gas3=(-9.95577e-03+5.23524e-03*z-1.45574e-04*y)*y*y
      gas4=(-3.20619e00-7.50405e-02*y+6.51564e-01*z)*z*z
      gas5=-6.62841e00+2.77112e-02*y
      gas6=(7.30762e00-7.68230e-02*y)*z
      gas7=(7.19421e-03-3.62463e-03*z+1.62777e-04*y)*y*y
      gas8=(-2.33161e00+3.04767e-02*y+1.66856e-01*z)*z*z
      gas9=(1.255324e02+2.015335e00*y-6.390747e01*z-6.515225e-01*y*z)
      if(kflag.eq.0) go to 240
      gas1r=-2.57909e-01
      gas2r=2.87201e-01*z
      gas3r=(-1.991154e-02+1.047048e-02*z-4.36722e-04*y)*y
      gas4r=-7.50405e-02*z*z
      gas5r=2.77112e-02
      gas6r=-7.6823e-02*z
      gas7r=(1.438842e-02-7.24926e-03*z+4.88331e-04*y)*y
      gas8r=3.04767e-02*z*z
      gas9r=2.015335e00-6.515225e-01*z
      gas2e=gas2/z
      gas3e=5.23524e-03*y*y
      gas4e=(-6.41238e00-1.50081e-01*y+1.954692e00*z)*z
      gas6e=gas6/z
      gas7e=-3.62463e-03*y*y
      gas8e=(-4.66322e00+6.09534e-02*y+5.00568e-01*z)*z
      gas9e=-6.390747e01-6.515225e-01*y
      go to 240
180   if (z.gt.2.95) go to 190
      gas1=-2.26460e00-7.82263e-02*y
      gas2=(4.90497e00+7.18096e-02*y)*z
      gas3=(-3.06443e-03+1.74209e-03*z+2.84214e-05*y)*y*y
      gas4=(-2.24750e00-1.31641e-02*y+3.33658e-01*z)*z*z
      gas5=-1.47904e01-1.76627e-01*y
      gas6=(1.35036e01+8.77280e-02*y)*z
      gas7=(-2.13327e-03+7.15487e-04*z+7.30928e-05*y)*y*y
      gas8=(-3.95372e00-8.96151e-03*y+3.63229e-01*z)*z*z
      gas9=(1.788542e02+6.317894e00*y-6.756741e01*z-2.460060e00*y*z)
      if(kflag.eq.0) go to 240
      gas1r=-7.82263e-02
      gas2r=7.18096e-02*z
      gas3r=(-6.12886e-03+3.48418e-03*z+8.52642e-05*y)*y
      gas4r=-1.31641e-02*z*z
      gas5r=-1.76627e-01
      gas6r=8.7728e-02*z
      gas7r=(-4.26654e-03+1.430974e-03*z+2.192784e-04*y)*y
      gas8r=-8.96151e-03*z*z
      gas9r=6.317894e00-2.46006e00*z
      gas2e=gas2/z
      gas3e=1.74209e-03*y*y
      gas4e=(-4.495e00-2.63282e-02*y+1.000974e00*z)*z
      gas6e=gas6/z
      gas7e=7.15487e-04*y*y
      gas8e=(-7.90744e00-1.792302e-02*y+1.089687e00*z)*z
      gas9e=-6.756741e01-2.46006e00*y
      go to 240
190   if(z.gt.3.4e00) write(6,1000) rho2,e2
      gas1=-1.66904e01-2.58318e-01*y
      gas2=(1.78350e01+1.54898e-01*y)*z
      gas3=(-9.71263e-03+3.97740e-03*z+9.04300e-05*y)*y*y
      gas4=(-5.94108e00-2.01335e-02*y+6.60432e-01*z)*z*z
      gas5=8.54690e01+1.17554e01*y
      gas6=(-7.21760e01-7.15723e00*y)*z
      gas7=(-4.16150e-02+1.38147e-02*z+5.45184e-04*y)*y*y
      gas8=(2.01758e01+1.08990e00*y-1.86438e00*z)*z*z
      gas9=(2.883262e02+1.248536e01*y-8.816985e01*z-3.720309e00*y*z)
      if(kflag.eq.0) go to 240
      gas1r=-2.58318e-01
      gas2r=1.54898e-01*z
      gas3r=(-1.942526e-02+7.9548e-03*z+2.7129e-04*y)*y
      gas4r=-2.01335e-02*z*z
      gas5r=1.17554e01
      gas6r=-7.15723e00*z
      gas7r=(-8.323e-02+2.76294e-02*z+1.635552e-03*y)*y
      gas8r=1.0899e00*z*z
      gas9r=1.248536e01-3.720309e00*z
      gas2e=gas2/z
      gas3e=3.9774e-03*y*y
      gas4e=(-1.188216e01-4.0267e-02*y+1.981296e00*z)*z
      gas6e=gas6/z
      gas7e=1.38147e-02*y*y
      gas8e=(4.03516e01+2.1798e00*y-5.59314e00*z)*z
      gas9e=-8.816985e01-3.720309e00*y
      go to 240
200   if(z.gt.0.65e00) go to 210
      gamm=1.3988e00
      go to 250
210   if(z.gt.1.7e00) go to 220
      gas1=1.37062e00+1.29673e-02*y
      gas2=(1.11418e-01-3.26912e-02*y)*z
      gas3=(1.06869e-03-2.00286e-03*z+2.38305e-04*y)*y*y
      gas4=(-1.06133e-01+1.90251e-02*y+3.02210e-03*z)*z*z
      gamm=gas1+gas2+gas3+gas4
      if(kflag.eq.0) go to 260
      gas1r=1.29673e-02
      gas2r=-3.26912e-02*z
      gas3r=(2.13738e-03-4.00572e-03*z+7.14915e-04*y)*y
      gas4r=1.90251e-02*z*z
      gas2e=gas2/z
      gas3e=-2.00286e-03*y*y
      gas4e=(-2.12266e-01+3.80502e-02*y+9.0663e-03*z)*z
      gammr=gas1r+gas2r+gas3r+gas4r
      gamme=gas2e+gas3e+gas4e
      go to 260
220   if(z.gt.2.35) go to 230
      gas1=3.43846e-02-2.33584e-01*y
      gas2=(2.85574e00+2.59787e-01*y)*z
      gas3=(-10.89927e-03+4.23659e-03*z+3.85712e-04*y)*y*y
      gas4=(-1.94785e00-6.73865e-02*y+4.08518e-01*z)*z*z
      gas5=-4.20569e00+1.33139e-01*y
      gas6=(4.51236e00-1.66341e-01*y)*z
      gas7=(1.67787e-03-1.10022e-03*z+3.06676e-04*y)*y*y
      gas8=(-1.35516e00+4.91716e-02*y+7.52509e-02*z)*z*z
      gas9=(1.757042e02-2.163278e00*y-8.833702e01*z+1.897543e00*y*z)
      if(kflag.eq.0) go to 240
      gas1r=-2.33584e-01
      gas2r=2.59787e-01*z
      gas3r=(-2.179854e-02+8.47318e-03*z+1.157136e-03*y)*y
      gas4r=-6.73865e-02*z*z
      gas5r=1.33139e-01
      gas6r=-1.66341e-01*z
      gas7r=(3.35574e-03-2.20044e-03*z+9.20028e-04*y)*y
      gas8r=4.91716e-02*z*z
      gas9r=-2.163278e00+1.897543e00*z
      gas2e=gas2/z
      gas3e=4.23659e-03*y*y
      gas4e=(-3.8957e00-1.34773e-01*y+1.225554e00*z)*z
      gas6e=gas6/z
      gas7e=-1.10022e-03*y*y
      gas8e=(-2.71032e00+9.83432e-02*y+2.257527e-01*z)*z
      gas9e=-8.833702e01+1.897543e00*y
      go to 240
230   if(z.gt.2.9e00) write(6,1000) rho2,e2
      gas1=-1.70633e00-1.48403e-01*y
      gas2=(4.23104e00+1.37290e-01*y)*z
      gas3=(-9.10934e-03+3.85707e-03*z+2.69026e-04*y)*y*y
      gas4=(-1.97292e00-2.81830e-02*y+2.95882e-01*z)*z*z
      gas5=3.41580e01-1.89972e01*y
      gas6=(-4.0858e01+1.30321e01*y)*z
      gas7=(-8.01272e-01+2.75121e-01*z-1.77969e-04*y)*y*y
      gas8=(1.60826e01-2.23386e00*y-2.08853e00*z)*z*z
      gas9=(2.561323e02+1.737089e02*y-9.058890e01*z-5.838803e01*y*z)
      if(gas9.gt.30.e00) gas9=30.e00
      if(gas9.lt.-30.e00) gas9=-30.e00
      if(kflag.eq.0) go to 240
      gas1r=-1.48403e-01
      gas2r=1.3729e-01*z
      gas3r=(-1.821868e-02+7.71414e-03*z+8.07078e-04*y)*y
      gas4r=-2.8183e-02*z*z
      gas5r=-1.89972e01
      gas6r=1.30321e01*z
      gas7r=(-1.602544e00+5.50242e-01*z-5.33907e-04*y)*y
      gas8r=-2.23386e00*z*z
      gas9r=1.737089e02-5.838803e01*z
      gas2e=gas2/z
      gas3e=3.85707e-03*y*y
      gas4e=(-3.94584e00-5.6366e-02*y+8.87646e-01*z)*z
      gas6e=gas6/z
      gas7e=2.75121e-01*y*y
      gas8e=(3.21652e01-4.46772e00*y-6.26559e00*z)*z
      gas9e=-9.05889e01-5.838803e01*y
240   gas9=exp(gas9)
      gamm=gas1+gas2+gas3+gas4+(gas5+gas6+gas7+gas8)/(1.+gas9)
      if(kflag.eq.0) go to 260
      gammr=gas1r+gas2r+gas3r+gas4r+(gas5r+gas6r+gas7r+gas8r)/(1.+gas9)-(gas5+gas6+gas7+gas8)*gas9r*gas9/((1.+gas9)**2)
      gamme=gas2e+gas3e+gas4e+(gas6e+gas7e+gas8e)/(1.+gas9)-(gas5+gas6+gas7+gas8)*gas9e*gas9/((1.+gas9)**2)
      go to 260
250   continue
      if(kflag.eq.0) go to 260
      gammr=0.0e00
      gamme=0.0e00
260   p2t=(gamm-1)*e2*rho2
!      write(4,*)e2
!      write(4,*)rho2
!      write(4,*)p2t
      if(kflag.eq.0) go to 270
      gammr=gammr/2.302585e00
      gamme=gamme/2.302585e00
      asq=e2*((gamm-1.e00)*(gamm+gamme)+gammr)
      a2=sqrt(asq)
270   if(iflag) 10,280,290
280   if(jflag) 30,40,10
290   if(jflag) 60,70,10
300   x=dlog10(p2t/p0)
      y=dlog10(rho2/r0)
      z1=x-y
      if (y.gt.-0.5e00) go to 400
      if(y.gt.-4.5e00) go to 350
      if(z1.gt.0.25e00) go to 310
      t2=p2t/(gascon*rho2)
      return
310   if(z1.gt.0.95e00) go to 320
      gas1=1.44824e-01+1.36744e-02*y
      gas2=(1.17099e-01-8.22299e-02*y)*z1
      gas3=(-6.75303e-04-1.47314e-03*z1-7.90851e-05*y)*y*y
      gas4=(1.3937e00+6.83066e-02*y-6.65673e-01*z1)*z1*z1
      tnon=gas1+gas2+gas3+gas4
      go to 450
320   if(z1.gt.1.4e00) go to 330
      gas1=-9.325e00-9.32017e-01*y
      gas2=(2.57176e01+1.61292e00*y)*z1
      gas3=(-3.00242e-02+2.62959e-02*z1-2.77651e-04*y)*y*y
      gas4=(-2.1662e01-6.81431e-01*y+6.26962e00*z1)*z1*z1
      gas5=-3.38534e00+1.82594e-01*y
      gas6=(1.84928e-01-7.01109e-01*y)*z1
      gas7=(1.10150e-02-1.60570e-02*z1+1.57701e-05*y)*y*y
      gas8=(5.4702e00+4.11624e-01*y-2.81498e00*z1)*z1*z1
      gas9=exp(-3.887015e01-2.908228e01*y+4.070557e01*z1+2.682347e01*y*z1)
      go to 440
330   if( z1.gt.1.95e00) go to 340
      gas1=-1.93082e01-1.54557e00*y
      gas2=(3.69035e01+1.92214e00*y)*z1
      gas3=(-3.59027e-02+2.31827e-02*z1-2.01327e-04*y)*y*y
      gas4=(-2.20440e01-5.80935e-01*y+4.43367e00*z1)*z1*z1
      gas5=-3.83069e00+1.32864e-01*y
      gas6=(-3.91902e00-6.79564e-01*y)*z1
      gas7=(6.06341e-04-8.12997e-03*z1-1.61012e-04*y)*y*y
      gas8=(7.24632e00+3.15461e-01*y-2.17879e00*z1)*z1*z1
      gas9=exp(2.08e01-2.56e01*y+1.0e00*z1+1.80e01*y*z1)
      go to 440
340   gas1=-2.59721e01-1.77419e00*y
      gas2=(3.62495e01+1.55383e00*y)*z1
      gas3=(-4.51359e-02+2.43648e-02*z1+1.2804e-04*y)*y*y
      gas4=(-1.59988e01-3.17807e-01*y+2.40584e00*z1)*z1*z1
      gas5=-1.81433e01+1.54896e-01*y
      gas6=(1.26582e01-3.66275e-01*y)*z1
      gas7=(3.24496e-02-1.66385e-02*z1+3.02177e-04*y)*y*y
      gas8=(-1.41759e00+1.11241e-01*y-3.10983e-01*z1)*z1*z1
      gas9=exp(1.115884e02-6.452606e00*y-5.337863e01*z1+2.026986e00*y*z1)
      go to 440
350   if(z1.gt.0.25e00) go to 360
      t2=p2t/(gascon*rho2)
      return
360   if(z1.gt.0.95e00) go to 370
      gas1=2.94996e-02+7.24997e-03*y
      gas2=(7.81783e-01-3.27402e-02*y)*z1
      gas3=(3.23357e-04-9.69989e-04*z1-8.93240e-06*y)*y*y
      gas4=(3.95198e-01+2.92926e-02*y-2.12182e-01*z1)*z1*z1
      tnon=gas1+gas2+gas3+gas4
      go to 450
370   if (z1.gt.1.4e00) go to 380
      gas1=-5.53324e00-3.53749e-01*y
      gas2=(1.63638e01+5.87547e-01*y)*z1
      gas3=(-1.16081e-02+7.99571e-03*z1-2.79316e-04*y)*y*y
      gas4=(-1.41239e01-2.35146e-01*y+4.28891e00*z1)*z1*z1
      gas5=9.07979e00+1.01308e00*y
      gas6=(-2.29428e01-1.52122e00*y)*z1
      gas7=(3.78390e-02-2.63115e-02*z1+5.46402e-04*y)*y*y
      gas8=(1.95657e01+5.73839e-01*y-5.63057e00*z1)*z1*z1
      gas9=exp(7.619803e01-1.501155e01*y-6.770845e01*z1+1.273147e01*y*z1)
      go to 440
380   if (z1.gt.2.00e00) go to 390
      gas1=-1.13598e01-1.02049e00*y
      gas2=(2.22793e01+1.24038e00*y)*z1
      gas3=(-3.10771e-02+1.92551e-02*z1-2.69140e-04*y)*y*y
      gas4=(-1.31512e01-3.62875e-01*y+2.64544e00*z1)*z1*z1
      gas5=8.72852e00+1.27564e00*y
      gas6=(-1.79172e01-1.52051e00*y)*z1
      gas7=(4.91264e-02-2.81731e-02*z1+5.23383e-04*y)*y*y
      gas8=(1.16719e01+4.45413e-01*y-2.45584e00*z1)*z1*z1
      gas9=exp(1.84792e02+9.583443e00*y-1.020835e02*z1-4.166727e00*y*z1)
      go to 440
390   gas1=-1.76079e01-1.26579e00*y
      gas2=(2.48544e01+1.09442e00*y)*z1
      gas3=(-3.65534e-02+1.54346e-02*z1-4.59822e-04*y)*y*y
      gas4=(-1.08166e01-2.27803e-01*y+1.60641e00*z1)*z1*z1
      gas5=2.60669e01+2.31791e00*y
      gas6=(-3.22433e01-1.82645e00*y)*z1
      gas7=(4.94621e-02-1.85542e-02*z1+5.04815e-04*y)*y*y
      gas8=(1.33829e01+3.59744e-01*y-1.86517e00*z1)*z1*z1
      gas9=exp(3.093755e02+1.875018e01*y-1.375004e02*z1-8.333418e00*y*z1)
      go to 440
400   if (z1.gt.0.25e00) go to 410
      t2=p2t/(gascon*rho2)
      return
410   if (z1.gt.0.95e00) go to 420
      gas1=-2.94081e-03+5.73915e-04*y
      gas2=(9.88883e-01-3.71241e-03*y)*z1
      gas3=(1.12387e-04-3.76528e-04*z1+1.76192e-05*y)*y*y
      gas4=(2.86656e-02+4.56059e-03*y-1.99498e-02*z1)*z1*z1
      tnon=gas1+gas2+gas3+gas4
      go to 450
420   if (z1.gt.1.45e00) go to 430
      gas1=1.32396e00+8.52771e-02*y
      gas2=(-3.24257e00-2.00937e-01*y)*z1
      gas3=(5.68146e-03-6.85856e-03*z1+1.98366e-04*y)*y*y
      gas4=(4.53823e00+1.18123e-01*y-1.6246e00*z1)*z1*z1
      gas5=-5.26673e-01-1.58691e-01*y
      gas6=(2.61600e00+3.16356e-01*y)*z1
      gas7=(-1.90755e-02+1.70124e-02*z1-5.58398e-04*y)*y*y
      gas8=(-3.3793e00-1.52212e-01*y+1.30757e00*z1)*z1*z1
      gas9=exp(1.442206e02-2.544727e01*y-1.277055e02*z1+2.236647e01*y*z1)
      go to 440
430   gas1=-1.60643e00-5.07368e-02*y
      gas2=(3.95872e00+3.69383e-02*y)*z1
      gas3=(-1.59378e-03+1.06057e-03*z1+6.53278e-05*y)*y*y
      gas4=(-1.71201e00+9.25124e-03*y+2.71039e-01*z1)*z1*z1
      gas5=1.80476e01+1.62964e00*y
      gas6=(-2.73124e01-1.57430e00*y)*z1
      gas7=(5.85277e-02-2.77313e-02*z1+1.16146e-03*y)*y*y
      gas8=(1.36342e01+3.70714e-01*y-2.23787e00*z1)*z1*z1
      gas9=exp(1.292515e02+1.360552e00*y-7.07482e01*z1+1.360532e00*y*z1)
440   tnon=gas1+gas2+gas3+gas4+(gas5+gas6+gas7+gas8)/(1.+gas9)
450   t2=(10.**tnon)*t0
1000  format(/20x,48hwarning]  outside of validity range of curve fit,/,20x,5hrho =,1pe15.8,5x,3he =,1pe15.8,/)
      return
      end subroutine tgas1
	 
	 end module srinivasan