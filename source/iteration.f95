     module iteration
	 
	     use srinivasan
		 
	 contains
	 
	 !subroutine for the iteration procedure to equilibrium air properties using srinivasan correlations
      subroutine normiter (p1,h1,rho1,v1,p2,ro1ro2,h2,rho2,e2)
      implicit none
      doubleprecision::p1,rho1,v1,h1
      doubleprecision::p2,h2,rho2,e2,a2,t2
      doubleprecision::p2t,cc,dd
      doubleprecision::ro1ro2,h2h1
      doubleprecision::delpn,delpo,ro1ro2o,ro1ro2n,error,tol,alfa,p2m
      integer::k,mflag
!
      
     
      mflag=0
      tol=1.0e-9
      alfa=1.
      ro1ro2=0.1
!
      cc=rho1*v1**2
      dd=v1**2/(2.*h1)
!     
      do 150 k=1,50
!
      p2m=p1+cc*(1.-ro1ro2)
	  h2h1=1.+dd*(1.-ro1ro2**2)
!
      h2=h2h1*h1
      rho2=rho1/ro1ro2
      e2=h2-p2m/rho2
!
      call tgas1 (e2,rho2,p2t,a2,t2,mflag) !correlations for finding pressure, temperature and soundspeed as function of density and internal energy
!
      delpn=p2m-p2t
!
      if(k.eq.1) then
	             delpo=delpn
	             ro1ro2o=ro1ro2
	             ro1ro2=1.25*ro1ro2
	             go to 150
	             endif
!
      error=abs(delpn/p2m)
!
      if(error.gt.tol) then
	                   ro1ro2n=ro1ro2+(delpn/(delpo-delpn))*(ro1ro2-ro1ro2o)
	                   ro1ro2o=ro1ro2
	                   ro1ro2=alfa*ro1ro2n+(1.-alfa)*ro1ro2
	                   delpo=delpn
	                   else
	                   go to 200
	                   endif
  150 continue
!
      stop
!
  200 p2=p2t
      return
      end subroutine normiter

!__________________________________________________________________________________________________
     end module iteration