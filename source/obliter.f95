     module obliter
!module containing the secant method and Beta-Theta-Mach functions for oblique shock calculations

     use iteration
	 
     contains

!     subroutine for calculating beta iteratively through the secant method for perfect gas
      subroutine secant (dl,x0,dx,gama,m1,thetar,thetamax)
      implicit none
      integer::istep
      doubleprecision, intent (inout) :: x0,dx
      doubleprecision:: x1,x2,d,fx0,fx1
      doubleprecision, intent (in) :: dl
      doubleprecision:: gama,m1,thetar,thetamax
!
      istep = 0
      fx0=fx(x0,gama,m1,thetar)
      x1=x0+dx
!
      do while (abs(dx).gt.dl)
         fx1= fx(x1,gama,m1,thetar)
         d  = fx1 - fx0
         x2 = x1-fx1*(x1-x0)/d
         x0 = x1
         fx0= fx1
         x1 = x2
         dx = x1-x0
         istep = istep+1
         if (istep>100) then
         write(2,*)''
         write(2,*)''
         write(2,*)'convergence error. see error_log.dat for detailed information.'
         write(4,"(/,10x,'exceeded maximum number of',1x,i3,1x,'iterations to find value of beta!')")istep
         if (thetar>thetamax) then
      write(4,"(/,10x,'given value for deflection angle is greater than allowed for input conditions:',f5.2,1x,'>',1x,f5.2,/)")&
     &thetar*180/3.14159,thetamax*180/3.14159
            write(4,*)'         this condition generates detached shock, verify your input parameters and try again.'
         else
         write(4,*)'         it is possible given value of theta exceeds maximum value for oblique shock, generating'
         write(4,*)'         a detached shock wave. verify the input parameters.'
!
	                   stop
                       endif
                       endif
      end do
      end subroutine secant
!____________________________________________________________________________________________________
!
!     subroutine for calculating beta iteratively for equilibrium air
      subroutine secant2 (vn1,vn2,v1,p1,h1,rho1,thetar,beta,y0,p2,ro1ro2,h2,rho2,e2)
      implicit none
      integer::istep
      double precision::p2,h2,rho2,e2,ro1ro2
      double precision::y0,dy
      double precision::y1,y2,d,fy0,fy1
      double precision::dl
      double precision::vn1,vn2,v1,p1,h1,rho1,thetar,beta
!
      dl=1.0e-09
      istep = 0
      fy0 = fy(y0,vn1,vn2,thetar)
      y1 = 0.9*beta-0.1*thetar 
      dy = y1 - y0
!      
      do while (abs(dy).gt.dl)
         vn1 = v1*sin(y1)
         call normiter(p1,h1,rho1,vn1,p2,ro1ro2,h2,rho2,e2)
         vn2 = vn1*ro1ro2
         fy1 = fy(y1,vn1,vn2,thetar)
         d  = fy1-fy0
         y2 = y1-fy1*(y1-y0)/d
         y0 = y1
         y1 = y2
         dy = y1-y0
         fy0 = fy1
         istep = istep+1
         if (istep>100) then
         write(3,*)'convergence error. see error_log.dat for detailed information.'
         write(4,"(/,10x,'exceeded maximum number of',1x,i3,1x,'iterations to find value of beta!')")istep
         write(4,*)'         it is possible given value of theta exceeds maximum value for oblique shock, generating'
         write(4,*)'         a detached shock wave. verify the input parameters.'
	                   stop
                       endif
      end do
      end subroutine secant2
!____________________________________________________________________________________________________
!
!     function beta-theta-mach for oblique shock waves for perfect gas
      function fx(x,gama,m1,thetar) result (f)
      implicit none
      doubleprecision::f,gama,m1,thetar
      doubleprecision, intent (in) :: x
      f=tan(thetar)-((2./dtan(x))*((m1**2.*(dsin(x))**2.-1.)/(m1**2.*(gama+dcos(2.*x))+2.)))
      end function fx
!_____________________________________________________________________________________________________
!
!     function beta-theta for equilibrium air oblique shock waves
      function fy(y,vn1,vn2,thetar) result (f)
      implicit none
      doubleprecision::f,vn1,vn2,thetar
      doubleprecision, intent (in) :: y
      f=(vn2/vn1)*dtan(y)-dtan(y-thetar)
      end function fy
!______________________________________________________________________________________________________	 
	 end module obliter