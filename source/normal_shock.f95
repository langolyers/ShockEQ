     module normal_shock

	 use srinivasan
	 use iteration
	 
	 contains
	 
	 !     subroutine for normal shock relations for perfect gas
      subroutine norm1 (p1,t1,p01,m1,rho1,v1,gama,r,cp)
      implicit none
      doubleprecision::p1,t1,rho1,m1,v1,p01
      doubleprecision::p2,p02,h2,t2,rho2,a2,m2,v2,ds
      doubleprecision::p2p1,t2t1,rho21
      doubleprecision::gama,r,cp
!      
         p2p1=1.+(2.*gama/(gama+1.))*(m1*m1-1.)
         rho21=m1*m1*(gama+1.)/(2.+(gama-1.)*m1*m1)
         t2t1=p2p1/rho21
         p2=p1*p2p1
         rho2=rho1*rho21
         v2=v1/rho21
         t2=t1*t2t1
!
         ds=cp*dlog(t2t1)-r*dlog(p2p1)
         p02=p01*dexp(-ds/r)
         h2=cp*t2
!
         a2=sqrt(gama*r*t2)
         m2=v2/a2
!
      write(2,*)''
      write(2,*)'        normal shock with calorically perfect gas'
      write(2,*)''
      write(2,*)'                       p2/p1                t2/t1             rho2/rho1'
      write(2,"('ratios',17x,f6.3,15x,f6.3,13x,f9.3)")p2p1,t2t1,rho21
      write(2,*)'--------------------------------------------------------------------------------------'
      write(2,*)''
      write(2,*)'                     p2 [pa]               t2 [k]          rho2 [kg/m3]'
      write(2,"(19x,f10.0,15x,f6.1,10x,f12.5)")p2,t2,rho2
      write(2,*)''
      write(2,*)'                    v2 [m/s]                   m2              a2 [m/s]' 
      write(2,"(21x,f8.2,17x,f4.2,14x,f8.2)")v2,m2,a2
      write(2,*)''
      write(2,*)'                   h2 [j/kg]'
      write(2,"(18x,f10.0)")h2
      write(2,*)''
      write(2,*)'          total pressure [pa]                  entropy increase [j/kgk]' 
      write(2,"(10x,f19.0,36x,f6.0)")p02,ds
!
      return
      end subroutine norm1
!_____________________________________________________________________________________________________
!
!subroutine for normal shock wave with equilibrium air using srinivasan correlations
      subroutine norm2 (p1,t1,rho1,v1,e1,h1)
	  implicit none
      doubleprecision::p1,t1,rho1,v1,e1,h1
      doubleprecision::p2,h2,t2,rho2,v2,e2,m2,a2
      doubleprecision::p2p1,t2t1,rho21,ro1ro2,e2e1,h2h1,p2t
      integer::mflag
!

	  call normiter(p1,h1,rho1,v1,p2,ro1ro2,h2,rho2,e2)
!
      mflag=3
      call tgas1 (e2,rho2,p2t,a2,t2,mflag)  !correlations for temperature and soundspeed as a function of density and pressure
!
      p2p1=p2/p1
      t2t1=t2/t1
      e2e1=e2/e1
      h2h1=h2/h1
      rho21=1./ro1ro2
      v2=ro1ro2*v1
      m2=v2/a2
!
      write(3,*)''
      write(3,*)'        normal shock with air in chemical equilibrium'
      write(3,*)''
      write(3,*)'                       p2/p1                t2/t1             rho2/rho1            h2/h1            e2/e1'
      write(3,"('ratios',17x,f6.3,15x,f6.3,13x,f9.3,11x,f6.3,11x,f6.3)")p2p1,t2t1,rho21,h2h1,e2e1
      write(3,*)'--------------------------------------------------------------------------------------'
      write(3,*)''
      write(3,*)'                     p2 [pa]               t2 [k]          rho2 [kg/m3]'
      write(3,"(19x,f10.0,15x,f6.1,10x,f12.5)")p2,t2,rho2
      write(3,*)''
      write(3,*)'                    v2 [m/s]                   m2              a2 [m/s]' 
      write(3,"(21x,f8.2,17x,f4.2,14x,f8.2)")v2,m2,a2
      write(3,*)''
      write(3,*)'                   h2 [j/kg]                   e2'
      write(3,"(19x,f10.0,11x,f10.0)")h2,e2
      write(3,*)''
!
      return
      end subroutine norm2
!_____________________________________________________________________________________________________
     end module normal_shock